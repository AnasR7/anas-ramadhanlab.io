var text = document.querySelector('.text');
var percent = document.querySelector('.percent');
var progress = document.querySelector('.progress');
var count = 4;
var per = 16;
var loading = setInterval(animate, 50);
function animate()
{
  if (count == 100 && per == 400)
  {
    text.textContent = "Welcome";
    text.style.fontSize = "40px";
    text.style.color = "#8CD9AA";
    text.classList.add("add");
    clearInterval(loading);
  }
  else
  {
    per = per + 4;
    count = count + 1;
    progress.style.width = per + 'px';
    percent.textContent = count + '%';
  }
}
window.addEventListener('load', () => {
  const loading_bg = document.querySelector('.loading_bg');
  loading_bg.classList.add('loading-finish');
});
